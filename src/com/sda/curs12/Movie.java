package com.sda.curs12;

import java.time.Duration;

public class Movie extends Video {

    VideoType type;


    public Movie(String name, VideoType type, Duration length, VideoQuality videoQuality ) {
        super(name,  length, videoQuality);
        this.type = type;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "type=" + type +
                "} " + super.toString();
    }
    public void play() {
        System.out.println("Playing movie: " + this);
    }
}
