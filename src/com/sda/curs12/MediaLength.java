package com.sda.curs12;

import java.time.Duration;

public class MediaLength extends Media{

    Duration length;
    public MediaLength(String name, Duration length) {
        super(name);
        this.length = length;
    }

    @Override
    public String toString() {
        return
                " Length : " + length
                + super.toString();
    }
}
