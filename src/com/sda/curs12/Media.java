package com.sda.curs12;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;
import java.util.Scanner;

public class Media {
    private String name;
    private Double ranking ;
    private double rankingSum = 0;
    private int rankingCounter = 0;

    public Media(String name) {

        this.name = name;
    }

    @Override
    public String toString() {
        return
                "name='" + name + '\'' +
                        ", ranking=" + ranking + '\n';
    }

    public String getName() {
        return name;
    }

    public Double getRanking() {
        return ranking;
    }

    public void setRanking(double mark) {

            rankingCounter++;
            this.rankingSum = rankingSum + mark;
            this.ranking = rankingSum / rankingCounter;

    }

    public void addComment (String comment){
        try {
            FileWriter fileWriter = new FileWriter(String.format("%s.txt", name), true);
            fileWriter.write(String.format("%s \n", comment ));
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
     public void displayComment(){
         File file = new File(String.format("%s.txt", name));
         try {
             Scanner scanner =new Scanner(file);
             while (scanner.hasNextLine()){
                 System.out.println(scanner.nextLine());
             }
         } catch (FileNotFoundException e) {
             e.printStackTrace();
         }
     }

}
