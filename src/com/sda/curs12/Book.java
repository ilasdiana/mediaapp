package com.sda.curs12;

import java.time.Duration;

public class Book extends Media implements Readable {
    String author;
    int pageNumber;


    public Book(String author, String name, int pageNumber ) {
        super(name);
        this.author = author;
        this.pageNumber = pageNumber;
    }

    public String getAuthor() {
        return author;
    }

    @Override
    public String toString() {
        return "Book{" +
                super.toString() +
                "author='" + author + '\'' +
                ", pageNumber=" + pageNumber +
                '}';
    }

    @Override
    public void read() {
        System.out.println("Reading " + this);
    }
}
