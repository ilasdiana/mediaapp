package com.sda.curs12;
/*
O aplicatie catalog pt diferite tipuri de media (video, myusic, carti)
Video : nume, durata, gen (comedie actiune, drama) - ranking
Music: artist , nume, durata, ranking
Carti: autor, nume, numar de pagini, ranking
Constraint: nume unice (altfel error message)
Utilizatorii pot adauga mcomentarii la obiecte(salvar in fisier txt)
Utilizatorii pot cauta bazat pe nume
Utilizatorii pot vedea comentarii pt fiecare tip de media in parte
Acest ranking trebuie calculat in functia de notele date

 */

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        Movie harryPotter = new Movie("Harry Potter", VideoType.ACTION, Duration.ofMinutes(130), VideoQuality.FULLHD);
        harryPotter.addComment("Superb film!");
        harryPotter.addComment("Wow");
        harryPotter.setRanking(5);
        harryPotter.setRanking(3);
        harryPotter.setRanking(4);
        Music oneMoreTime = new Music("Britney Spears", "One more time", Duration.ofMinutes(3));
        oneMoreTime.addComment("Foarte tare");
        Book doctorSleep = new Book("Stephen King", "Doctor Sleep", 670);
        Book shining = new Book("Stephen King", "The Shining", 540);


        MediaPlatform mediaList = new MediaPlatform();
        mediaList.addMedia(oneMoreTime);
        mediaList.addMedia(harryPotter);
        mediaList.addMedia(doctorSleep);
        mediaList.addMedia(shining);
        mediaList.searchAndDisplay("Harry Potter");
        Movie harryPotter2 = new Movie("Harry Potter 2", VideoType.ACTION, Duration.ofMinutes(130), VideoQuality.HD);
        mediaList.addMedia(harryPotter2);
        YoutubeVideo video = new YoutubeVideo("Vlog", Duration.ofMinutes(34), VideoQuality.FULLHD, 1200);



        List<Playable> playableObjects = new ArrayList<>();

        playableObjects.add(video);
        playableObjects.add(harryPotter);
        playableObjects.add(oneMoreTime);
        playableObjects.add(harryPotter2);
        for (Playable playable : playableObjects){
            playable.play();
        }

    }
}
