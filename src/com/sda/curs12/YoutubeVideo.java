package com.sda.curs12;

import java.time.Duration;



public class YoutubeVideo extends Video {
    private int views;
    public YoutubeVideo(String name, Duration length, VideoQuality videoQuality, int views) {
        super(name, length, videoQuality);
        this.views = views;
    }


    @Override
    public String toString() {
        return "YoutubeVideo{} " + super.toString();
    }

    @Override
    public void play() {
        System.out.println("Playing youtube video : " + this);
    }
}
