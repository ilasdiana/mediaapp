package com.sda.curs12;

import java.util.ArrayList;
import java.util.List;

public class MediaPlatform {
    List<Media> mediaList = new ArrayList<>();

    public void addMedia(Media media) {
        Media mediaVerified = searchByName(media.getName());
        if (mediaVerified == null) {
            this.mediaList.add(media);
        } else {
            System.out.println(String.format(mediaVerified.getName() + " Already exists!"));
        }
    }


    @Override
    public String toString() {
        return "MediaPlatform{" +
                "mediaList=" + mediaList +
                '}';
    }

    public Media searchByName(String nameToBeFound) {
        for (Media media : mediaList) {
            if (media.getName().equals(nameToBeFound)) {
                return media;
            }
        }
        return null;
    }
    public void searchAndDisplay (String name){
        Media mediaSearched = searchByName(name);
        if( mediaSearched== null){
            System.out.println("Item not found");
        } else {
            System.out.println(mediaSearched);
        }
    }

    public List<Media> getMediaByType (Class type){
        List<Media> workingList = new ArrayList<>();
        for (Media media: this.mediaList){
            if (media.getClass().equals(type)){
                workingList.add(media);
            }
        }
        return workingList;
    }
    public void searchByAuthor(String name){
        List<Media> bookList = getMediaByType(Book.class);
        for (Media media : bookList){
            if (((Book) media).getAuthor().contains("King")){
                System.out.println(media);
            }
        }

    }
}
