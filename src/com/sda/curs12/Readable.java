package com.sda.curs12;

public interface Readable {
    void read();
}
